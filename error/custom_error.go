package error

import (
    "github.com/labstack/echo"
    "net/http"
)

type ApiError struct {
    Code    int    `json:"code"`
    Message string `json:"message"`
}

// CustomHTTPErrorHandler implements custom error handler for echo
func CustomHTTPErrorHandler(err error, c echo.Context) {
    code := http.StatusInternalServerError
    message := "Something went wrong"

    if he, ok := err.(*echo.HTTPError); ok {
        code = he.Code
        message = he.Message.(string)
    }

    c.Logger().Error(err)
    _ = c.JSON(code, ApiError{code, message})
}

