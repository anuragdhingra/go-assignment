# Notes

## Initial setup
- Run `docker-compose up -d` which will spin up three containers.

  `zenport_api`: app container running the go server @ port `9090`

  `zenpot_mysql`: mysql container for storing app data @ port `3306`

  `zenport_mysql_test`: mysql container for running integration tests @ port `3307`
  
  *Note*: Once these three containers are up and running, [gorm](https://gorm.io/), 
  the ORM used in the project will automatically migrate the required schemas 
  and the project should be ready to test the API's or to run tests by the default
  procedure as mentioned in [README.md](./README.md).
  
## Modifications
- **Additional directories added:**
  - `/config`: This stores the `environment.yml` config file for the project and
  	also maps these config properties for different envs namely `local` and `docker_local` 
  	back to objects that could easily be used inside our application.
  - `/error`: This folder is responsible for any custom error related implementation
  	for the project.
  - `/utils`: This folder holds any utility related functions or any helper methods.
  
- **Changes to original structure/code:**
  - The only change done with original code is to change the 
  `Fighter` interface method `getID()` to return `uint` instead of `string`.
  Reason being for this change is as `ID` field acts as a primary key for any 
  implementation of the above said interface, like `Knight`, it made more sense
  to have it as an auto-increment-able type, as `string` comes with its inherent 
  performance issues related to matching, indexing and joining over it.
  Even if we wanted a stringID, we could have it as a natural key, marking it as
  `UNIQUE` on the DB level.
  The above mentioned change, also caused the same change within use-case 
  or repository layer method definitions and to its related test implementation.
  
## Questions
- **What do you think of the initial project structure ?**

	- The initial project structure, closely resembled the clean architecture by Uncle bob,
	where the goal is to make your system independent of frameworks, databases, UI or
	any other external agency. Dividing your system into layers like entity or domain,
	interface, use-case or engine helps us to have control over external factors like
	what databases, communication protocols, UI etc. The goal is to make the system loosely
	coupled so we could easily switch out MongoDB for MySQL or use gRPC in place of REST or
	something else.

- **What you will improve from your solution ?**

	- Use different file structure for different handler implementations
	, currently included in with `adapter.go` which might not be feasible as handler
	types increase.
	- Having better configuration setup for different envs, currently for the ease,
	everything is included in one common file config file and is mounted to the application container.
	- Include unit testing for individual layers, adapters, engine and providers.
	- Having CI/CD in place to run unit tests and integrations test, include linting etc.
	Also for faster and automatic deployments.
	- Include manual migrations using any external package like `go-migrate` 
	or something than having automatic DB migrations via ORM.
	- Better error handling and logging in place. Have a custom `error` 
	implementation flexible to our needs.
	
- **For you, what are the boundaries of a service inside a "micro-service" architecture ?**

	- The most important boundary acc to me when deciding a micro-service is the single
	responsibility principle in the business context. It should result in a service,
	small enough for individual teams to work on it.
	- The boundary should adhere to not causing any data-consistency problems, between
	different services.For example, if eventual consistency is not what we aim, then its better
	to merge these boundaries in favor of single service is better in my experience.
	Also, have felt that, if the communication between two boundaries, feel more 
	than usual(nobody knows what is too much though) they usually belong to the same bounded context.
	- The services divided by these boundaries should be able to be deployed
	and maintained independently. If to deploy one service we need to redeploy some 
	other service too, it signals tight coupling between those services, which should
	be avoided when deciding the boundaries.
	
- **For you, what are the most relevant usage for SQL, NoSQL, key-value and document store databases ?**

	Honestly, I have mostly worked with SQL databases, but have also worked with NoSQL on some
	occasions, so the below opinions are based on my limited experience with NoSQL databases.
	- I have always felt while designing DB schemas and relations that not all data is always
	relational. For the kind of data that requires and lot of relations between different
	entities, SQL databases work the best. But, if there ain't a lot of many-to-many relationships
	between your entities then NoSQL databases might as well work great.
	Also if the kind of data being stored is very dynamic and doesnt fit well in
	a particular schema design then NoSQL works better.
	- Also, it depends a lot on what you decide on doing with the data stored, 
	if the need is to perform complicated queries that require JOINs between different
	tables/documents then SQL is the way to go. Also, if that isn't the case
	NoSQL databases like mongoDB work well by churning out better performance by
	not having any integrity checks in place thus great of analytical purposes.
	- About key-value databases, I've only worked with a couple of them. Redis, being
	the most popular key-value data store has usually been used in every application, I've
	worked on so for for in-memory cache management.
	Another one being DynamoDB, for invalidating authentication token being used over
	different devices. It usually works well when you want to store data that require too
	many state changes and also higher read throughput.
	
