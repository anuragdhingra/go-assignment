package database

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/zenport.io/go-assignment/domain"
)

type knightRepository struct {
	provider *Provider
}

// Find repository method to find knight record by ID
func (repository *knightRepository) Find(ID uint) (*domain.Knight, error) {
	var k domain.Knight
	err := repository.provider.db.Table("knights").Find(&k, "id = ?", ID).Error

	if gorm.IsRecordNotFoundError(err) {
		return nil, nil
	}

	return &k, err
}

// FindAll repository method to find all knight records
func (repository *knightRepository) FindAll() ([]*domain.Knight, error) {
	ks := make([]*domain.Knight, 0)
	err := repository.provider.db.Table("knights").Find(&ks).Error
	return ks, err
}

// Save repository method to create a new knight record
func (repository *knightRepository) Save(knight *domain.Knight) error {
	return repository.provider.db.Table("knights").Save(&knight).Error
}
