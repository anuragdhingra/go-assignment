package http

import (
	"fmt"
	"github.com/labstack/echo"
	"gitlab.com/zenport.io/go-assignment/config"
	"gitlab.com/zenport.io/go-assignment/domain"
	"gitlab.com/zenport.io/go-assignment/engine"
	"log"
	"net/http"
	"strconv"
)

type HTTPAdapter struct {
	server *echo.Echo
	engine engine.Engine
}

func (adapter *HTTPAdapter) Start() {
	log.Fatal(adapter.server.Start(config.Config.App.Port))
}

func (adapter *HTTPAdapter) Stop() {
	if err := adapter.server.Close(); err != nil {
		log.Fatal("Error stopping server:", err)
	}
	log.Println("Stopping server")
}

func NewHTTPAdapter(s *echo.Echo, e engine.Engine) *HTTPAdapter {
	adapter := &HTTPAdapter{server: s, engine: e}
	s.GET("/knight", adapter.getAllKnightsHandler)
	s.GET("/knight/:id", adapter.getKnightsHandler)
	s.POST("/knight", adapter.createKnightHandler)
	return adapter
}

func (adapter *HTTPAdapter) getKnightsHandler(c echo.Context) error {
	prm := c.Param("id")
	id, err := strconv.Atoi(prm)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, fmt.Sprintf("Invalid ID #%v", prm))
	}

	knight, err := adapter.engine.GetKnight(uint(id))
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, &knight)
}

func (adapter *HTTPAdapter) getAllKnightsHandler(c echo.Context) error {
	knights, err := adapter.engine.ListKnights()
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, &knights)
}

func (adapter *HTTPAdapter) createKnightHandler(c echo.Context) error {
	k := new(domain.Knight)
	err := c.Bind(k)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	err = c.Validate(k)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	err = adapter.engine.CreateKnight(k)
	if err != nil {
		return err
	}

	return c.NoContent(http.StatusCreated)
}

