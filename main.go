package main

import (
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gitlab.com/zenport.io/go-assignment/adapters/http"
	"gitlab.com/zenport.io/go-assignment/config"
	"gitlab.com/zenport.io/go-assignment/domain"
	"gitlab.com/zenport.io/go-assignment/engine"
	appErr "gitlab.com/zenport.io/go-assignment/error"
	"gitlab.com/zenport.io/go-assignment/providers/database"
	"gitlab.com/zenport.io/go-assignment/utils"
	"log"
	"net/url"
	"os"
	"os/signal"
	"syscall"
)

func init() {
	env := os.Getenv("ENV")

	if env == "" {
		env = "local"
	}

	config.SetEnvironment(env)
}

func main() {
	d := config.Config.Database
	connection := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", d.User, d.Password, d.Host, d.Port, d.DbName)
	val := url.Values{}
	val.Add("parseTime", "1")
	dsn := fmt.Sprintf("%s?%s", connection, val.Encode())
	db, err := gorm.Open("mysql", dsn)
	if err != nil {
		log.Fatal(err)
	}
	db.AutoMigrate(&domain.Knight{})

	provider := database.NewProvider(db)
	e := engine.NewEngine(provider)

	server := echo.New()

	// Register logger middleware
	server.Use(middleware.Logger())

	// Register custom error handler
	server.HTTPErrorHandler = appErr.CustomHTTPErrorHandler

	// Register validator
	server.Validator = &utils.CustomValidator{ Validator: validator.New() }

	adapter := http.NewHTTPAdapter(server, e)

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, syscall.SIGINT, syscall.SIGTERM)
	defer close(stop)

	adapter.Start()

	<-stop

	adapter.Stop()
	provider.Close()
}
