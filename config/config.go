package config

import (
    "io/ioutil"

    yaml "gopkg.in/yaml.v2"
)

var Config Conf

type Environment struct {
    Local       Conf `yaml:"local"`
    DockerLocal Conf `yaml:"docker_local"`
}

type Application struct {
    Port string `yaml:"port"`
}

type Conf struct {
    Database   Database    `yaml:"db"`
    App        Application `yaml:"app"`
}

type Database struct {
    User     string `yaml:"user"`
    Password string `yaml:"password"`
    Host     string `yaml:"host"`
    Port     string `yaml:"port"`
    DbName   string `yaml:"db_name"`
}

func SetEnvironment(env string) {

    buf, err := ioutil.ReadFile("config/environment.yml")

    if err != nil {
        panic(err)
    }

    var environment Environment

    err = yaml.Unmarshal(buf, &environment)
    if err != nil {
        panic(err)
    }

    switch env {
    case "local":
        Config = environment.Local
    case "docker_local":
        Config = environment.DockerLocal
    default:
        Config = environment.Local
    }

}
