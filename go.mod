module gitlab.com/zenport.io/go-assignment

go 1.13

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/go-playground/validator/v10 v10.3.0
	github.com/jinzhu/gorm v1.9.14
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0
	golang.org/x/net v0.0.0-20200707034311-ab3426394381 // indirect
	golang.org/x/sys v0.0.0-20200625212154-ddb9806d33ae // indirect
	gopkg.in/yaml.v2 v2.2.7
)
