package engine

import (
	"fmt"
	"github.com/labstack/echo"
	"net/http"

	"gitlab.com/zenport.io/go-assignment/domain"
)

// GetKnight use-case to get knight by ID
func (engine *arenaEngine) GetKnight(ID uint) (*domain.Knight, error) {
	fighter, err := engine.knightRepository.Find(ID)
	if err != nil {
		return nil, echo.NewHTTPError(http.StatusInternalServerError, "Something went wrong")
	}

	if fighter == nil {
		return nil, echo.NewHTTPError(http.StatusNotFound, fmt.Sprintf("Knight #%v not found.", ID))
	}

	return fighter, nil
}

// ListKnights use-case to get list of knights
func (engine *arenaEngine) ListKnights() ([]*domain.Knight, error) {
	fighters, err := engine.knightRepository.FindAll()
	if err != nil {
		return nil, echo.NewHTTPError(http.StatusInternalServerError, "Something went wrong")
	}

	return fighters, nil
}

// CreateKnight use-case to create a new knight resource
func (engine *arenaEngine) CreateKnight(knight *domain.Knight) error {
	err := engine.knightRepository.Save(knight)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, "Something went wrong")
	}

	return nil
}

// Fight use-case to simulate fight in an arena
func (engine *arenaEngine) Fight(fighter1ID uint, fighter2ID uint) domain.Fighter {
	fighter1, _ := engine.knightRepository.Find(fighter1ID)
	fighter2, _ := engine.knightRepository.Find(fighter2ID)

	return engine.arena.Fight(fighter1, fighter2)
}
