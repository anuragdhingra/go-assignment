package domain

type Fighter interface {
    GetID() uint
    GetPower() float64
}

type Knight struct {
    ID          uint   `json:"id" gorm:"PRIMARY KEY"`
    Name        string `json:"name" validate:"required" gorm:"NOT NULL"`
    Strength    int    `json:"strength" validate:"required" gorm:"NOT NULL"`
    WeaponPower int    `json:"weapon_power" validate:"required" gorm:"NOT NULL"`
}

func (k *Knight) GetID() uint {
    return k.ID
}

func (k *Knight) GetPower() float64 {
    return float64(k.Strength + k.WeaponPower)
}