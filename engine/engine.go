package engine

import "gitlab.com/zenport.io/go-assignment/domain"

type Engine interface {
	GetKnight(ID uint) (*domain.Knight, error)
	ListKnights() ([]*domain.Knight, error)
	CreateKnight(*domain.Knight) error
	Fight(fighter1ID uint, fighter2ID uint) domain.Fighter
}

type KnightRepository interface {
	Find(ID uint) (*domain.Knight, error)
	FindAll() ([]*domain.Knight, error)
	Save(knight *domain.Knight) error
}

type DatabaseProvider interface {
	GetKnightRepository() KnightRepository
}

type arenaEngine struct {
	arena            *domain.Arena
	knightRepository KnightRepository
}

func NewEngine(db DatabaseProvider) Engine {
	return &arenaEngine{
		arena:            &domain.Arena{},
		knightRepository: db.GetKnightRepository(),
	}
}
