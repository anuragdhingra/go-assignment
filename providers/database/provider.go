package database

import (
	"github.com/jinzhu/gorm"

	"gitlab.com/zenport.io/go-assignment/engine"
)

type Provider struct {
	db *gorm.DB
}

func (provider *Provider) GetKnightRepository() engine.KnightRepository {
	return &knightRepository{provider: provider}
}

func (provider *Provider) Close() {
	_ = provider.db.Close()
}

func NewProvider(db *gorm.DB) *Provider {
	return &Provider{db: db}
}
